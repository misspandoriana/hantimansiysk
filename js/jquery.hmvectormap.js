;(function($, window, document, undefined) {
    var pluginName = 'hmvectormap',
        defaults = {
            speed: 2000,
            sceneWidth: 1400,
            sceneHeight: 700,
            btnStartX: 1170,
            btnStartY: 80,
            btnWidth: 220,
            txtStartX: 560,
            txtStartY: 650,
            fadeSpeed: 300
        };

    var drawArrows = function(arrowsObj, body, nose, places, paper){
        var options = $this.options;
        $.each(arrowsObj, function(i, group){
            body[i] = [];
            nose[i] = [];
            $.each(group, function(key, item){
                body[i][key] = paper.path(item.start.body).attr(options.arrowBodyAttr).hide();
                nose[i][key] = paper.path(item.start.nose).attr(options.arrowNoseAttr).hide();
            });
        });
    };

    var createGreyFon = function(){
        var grayFon = $('<div/>', {'class': 'grayFon'}).hide().appendTo('body');
        return grayFon;
    };

    var createCloseBtn = function(){
        var closeBtn = $('<div/>', {'class': 'closeBtn'}).hide().appendTo($this.element);
        closeBtn.html('<i class="close white icon big"></i>');
        return closeBtn;
    };

    var getCloseBtnH = function(closeBtn){
        var closeBtnH = parseInt(closeBtn.css('top')) - 15;
        (closeBtnH < 0) ? closeBtnH = -closeBtnH : closeBtnH = closeBtnH;
        return closeBtnH;
    };

    var openMap = function(e){
        var options = $this.options;
        var data = e.data;
        var grayFon = data.grayFon;
        var mapBox = data.mapBox;
        var paper = data.paper;
        var closeBtn = data.closeBtn;
        var closeBtnH = data.closeBtnH;
        var fons = data.fons;
        fons.fon1.show();
        mapBox.show();
        grayFon.show();
        closeBtn.show();
        resizeScene(mapBox, paper, closeBtnH);
        grayFon.animate({opacity: '0.85'}, options.fadeSpeed);
        mapBox.animate({opacity: '1'}, options.fadeSpeed);

    };
    var closeMap = function(e){
        var options = $this.options;
        var data = e.data;
        var grayFon = data.grayFon;
        var mapBox = data.mapBox;
        var fons = data.fons;
        var dataMap = {
            'conturs': data.conturs,
            'arrowsObj': data.arrowsObj,
            'allGroups': data.allGroups,
            'placesObj': data.placesObj
        }
        grayFon.animate({opacity: '0'}, options.fadeSpeed, function(){$(this).hide()});
        mapBox.animate({opacity: '0'}, options.fadeSpeed, function(){
            hideAllInfotext(dataMap);
            hideAllArea(dataMap);
            hideAllArrows(dataMap);
            fons.fon1.hide();
            fons.fon2.hide();
            $(this).hide();
        });

    };

    var hideAllArrows = function(data){
        var body = data.conturs.body;
        var nose = data.conturs.nose;
        var arrowsObj = data.arrowsObj;
        $.each(arrowsObj, function(i, group){
            $.each(group, function(key, item){
                ((body[i][key]) && (body[i][key] !== undefined)) ? body[i][key].stop().attr('path', item.start.body).hide() : false;
                ((nose[i][key]) && (nose[i][key] !== undefined)) ? nose[i][key].stop().attr('path', item.start.nose).hide() : false;
            });
        });
    };

    var getGroup = function(href){
        if(href && (href != '')){
            var group = decodeURIComponent(href.substring(href.indexOf('#') + 1));
            if((!group) || (group === undefined)){
                group = null;
            }
        }else{
            var group = null;
        }
        return group;
    };

    var showInfotext = function(data){
        var group = data.group;
        var infotext = data.conturs.infotext;
        var text = infotext[group].show();
        text.stop().animate({opacity: 1}, 500);
    };

    var hideAllInfotext = function(data){
        var allGroups = data.allGroups;
        var infotext = data.conturs.infotext;
        $.each(allGroups, function(i, el){
            infotext[el].stop().attr({opacity: 0}).hide();
        });

    };

    var arrowAnimate = function(el, arrowsObj, group, item, places){
        var options = $this.options;
        var purpose = arrowsObj[group][item].end.area;
        var animate = Raphael.animation({'100%': {path: arrowsObj[group][item].end[el]}}, options.speed, 'easeInOut', function(){
            ((places[purpose]) && (places[purpose] != undefined)) ? places[purpose].show() : false;
        });
        return animate;
    };

    var showGroup = function(data){
        var group = data.group;
        var arrowsObj = data.arrowsObj;
        var body = data.conturs.body;
        var nose = data.conturs.nose;
        var places = data.conturs.places;
        if((arrowsObj[group]) && (arrowsObj[group] !== undefined)){
            $.each(arrowsObj[group], function(key, item){
                ((body[group][key]) && (body[group][key] !== undefined)) ? body[group][key].show().stop().animate(arrowAnimate('body', arrowsObj, group, key, places)) : false;
                ((nose[group][key]) && (nose[group][key] !== undefined)) ? nose[group][key].show().stop().animate(arrowAnimate('nose', arrowsObj, group, key, places)) : false;
            });
        }
    };

    var showGroupArea = function(data){
        var group = data.group;
        var arrowsObj = data.arrowsObj;
        var places = data.conturs.places;
        if((arrowsObj[group]) && (arrowsObj[group] !== undefined)){
            $.each(arrowsObj[group], function(key, item){
                var area = item.start.area;
                ((places[area]) && (places[area] != undefined)) ? places[area].show() : false;
            });
        }
    }

    var hideAllArea = function(data){
        var places = data.conturs.places;
        $.each(data.placesObj, function(key, item){
            places[key].hide();
        });
    }

    var handlerBtn = function(data){
        if((data.group) && (data.group !== undefined)){
            if(data.group == 'group1'){
                data.fons.fon1.show();
                data.fons.fon2.hide();
            }else{
                data.fons.fon2.show();
                data.fons.fon1.hide();
            }
            hideAllInfotext(data);
            showInfotext(data);
            hideAllArea(data);
            showGroupArea(data);
            hideAllArrows(data);
            showGroup(data);

        }
        return false;
    };

    var getAllGroups = function(mapList){
        var allGroups = [];
        $.each(mapList.find('li'), function(i, el){
            var link = $(el).children('a[data-role="mapGroup"]');
            var href = link.attr('href');
            var group = getGroup(href);
            allGroups.push(group);
        });
        return allGroups;
    };

    var bindBtnHandler = function(el, data){
        el.click(function(){
            var group = this.data('group');
            if(group && group !== undefined){
                data.group = group;
                handlerBtn(data);
            };
        });
    };

    var getStyle = function(dataStyle){
        var btnStyle;
        switch(dataStyle){
            case 1: {
                btnStyle = $this.options.btnStyle1;
                break;
            }
            case 2:{
                btnStyle = $this.options.btnStyle2;
                break;
            }
            case 3:{
                btnStyle = $this.options.btnStyle3;
                break;
            }
            default:{
                btnStyle = $this.options.btnStyle1;
                break;
            }
        }
        return btnStyle;
    }

    var labelBtn = function(btnObj, text, dataLevel, textattr){
        var coordX;
        var circle;
        var bbox = btnObj.getBBox();
        switch(dataLevel){
            case 1: {
                coordX = bbox.x + 10;
                circle = false;
                break;
            }
            case 2:{
                coordX = bbox.x + 30;
                circle = true;
                break;
            }
            default:{
                coordX = bbox.x + 10;
                circle = false;
                break;
            }
        }

        if (textattr == undefined){
            textattr = $this.options.textBtnStyle;
        };

        var textObj = btnObj.paper.text(coordX, bbox.y + bbox.height / 2, text).attr(textattr);
        if(circle){
            var circleObj = btnObj.paper.circle(bbox.x + 15, bbox.y + 20, 3).attr({fill: '#292424'});
        }
        return textObj;
    }

    var heightDependence = function(mapBox, winWidth, prop, closeBtnH){
        var mapBoxWidth = winWidth * 0.95;
        mapBox.width(mapBoxWidth);
        var mapBoxHeight = mapBoxWidth/prop + closeBtnH;
        var ret = {mapBoxHeight:mapBoxHeight, mapBoxWidth: mapBoxWidth};
        mapBox.height(ret.mapBoxHeight);
        return ret;
    };

    var widthDependence = function(mapBox, winHeight, prop, closeBtnH){
        var mapBoxHeight = (winHeight * 0.95) - closeBtnH;
        mapBox.height(mapBoxHeight);
        var mapBoxWidth = mapBoxHeight * prop;
        mapBox.width(mapBoxWidth);
        var ret = {mapBoxHeight:mapBoxHeight, mapBoxWidth: mapBoxWidth};
        return ret;
    };

    var resizeScene = function(mapBox, paper, closeBtnH){
        var options = $this.options, mapBoxWidth, mapBoxHeight, winWidth, winHeight, prop, widthDependenceReturn, heightDependenceReturn;
        winWidth = $(window).width();
        winHeight = $(window).height();

        if((winWidth > 100) && (winHeight > 100)){
            prop = options.sceneWidth / options.sceneHeight;
            (mapBox.width() > 0) ? mapBoxWidth = mapBox.width() : mapBoxWidth = options.sceneWidth;
            (mapBox.height() > 0) ? mapBoxHeight = mapBox.height() : mapBoxHeight = options.sceneHeight;

            if(winWidth <= options.sceneWidth){
                heightDependenceReturn = heightDependence(mapBox, winWidth, prop, closeBtnH);
                if(heightDependenceReturn){
                    mapBoxHeight = heightDependenceReturn.mapBoxHeight;
                    mapBoxWidth = heightDependenceReturn.mapBoxWidth;
                }

                if(mapBoxHeight > winHeight){
                    widthDependenceReturn  = widthDependence(mapBox, winHeight, prop, closeBtnH);
                    if(widthDependenceReturn){
                        mapBoxWidth = widthDependenceReturn.mapBoxWidth;
                        mapBoxHeight = widthDependenceReturn.mapBoxHeight;
                    }
                }
            };

            if(winHeight <= (options.sceneHeight + closeBtnH)){
                widthDependenceReturn  = widthDependence(mapBox, winHeight, prop, closeBtnH);
                if(widthDependenceReturn){
                    mapBoxWidth = widthDependenceReturn.mapBoxWidth;
                    mapBoxHeight = widthDependenceReturn.mapBoxHeight;
                }

                if(mapBoxWidth > winWidth){
                    heightDependenceReturn = heightDependence(mapBox, winWidth, prop, closeBtnH);
                    if(heightDependenceReturn){
                        mapBoxHeight = heightDependenceReturn.mapBoxHeight;
                        mapBoxWidth = heightDependenceReturn.mapBoxWidth;
                    }
                }
            };

            var ml = -(mapBoxWidth/2);
            var mt = -(mapBoxHeight/2);
            mapBox.css({
                'left': '50%',
                'marginLeft': (ml),
                'top': '50%',
                'marginTop': (mt)
            });

            paper.setViewBox(0, 0, options.sceneWidth, options.sceneHeight, true);
            paper.setSize('100%', '100%');
        }
    };

    var drawButtons = function(conturs, paper, arrowsObj, mapList, placesObj, fons){
        var options = $this.options;
        var startX = options.btnStartX;
        var startY = options.btnStartY;
        var btnWidth = options.btnWidth;
        var allGroups = getAllGroups(mapList);
        var data = {
            'conturs': conturs,
            'arrowsObj': arrowsObj,
            'allGroups': allGroups,
            'placesObj': placesObj,
            'fons': fons
        }

        $.each(mapList.find('li'), function(i, el){
            var link = $(el).children('a[data-role="mapGroup"]');
            var href = link.attr('href');
            var btn = link.children('[data-role="btn"]');
            var btnH = btn.data('h');
            var dataStyle = btn.data('style');
            var btnTxt = btn.html();
            var group = getGroup(href);
            var dataLevel = btn.data('level');
            var btnStyle = getStyle(dataStyle);
            var btn = paper.rect(startX, startY, btnWidth, btnH).attr(btnStyle).data('group', group);
            var text = labelBtn(btn, btnTxt, dataLevel).data('group', group);
            conturs.buttons.push(btn);

            bindBtnHandler(btn, data);
            bindBtnHandler(text, data);
            startY = startY + btnH + 5;
        });
    };

    var drawTitle = function(paper, titleTxt){
        var objTitle = paper.text(700, 40, titleTxt).attr($this.options.textTitle);
    };

    var drawPlaces = function(placesObj, places, paper){
        var options = $this.options;
        $.each(placesObj, function(i, item){
            var path = paper.path(item).attr(options.placeAttr);
            places[i] = path;
            path.hide();
        });
    };

    var drawInfoText = function(infotext, mapList, paper){
        var options = $this.options;
        var startX = options.txtStartX;
        var startY = options.txtStartY;

        $.each(mapList.find('li'), function(i, el){
            var link = $(el).children('a[data-role="mapGroup"]');
            var href = link.attr('href');
            var group = getGroup(href);

            var txt = link.children('[data-role="txt"]').html();

            if((txt) && (txt !== undefined)){
                var txt = paper.text(startX, startY, txt).attr($this.options.textInfoStyle);
                infotext[group] = txt;
                txt.hide();
            }
        });
    };

     var Plugin = function(element, options){
         $this = this;
         $this.element = element;
         $this.options = $.extend({}, defaults, options);
         $this.options.placeAttr = {stroke: '#39e5df', 'stroke-width': '3', 'z-index': 5};
         $this.options.arrowBodyAttr = {stroke: '#662D91', 'stroke-width': '4', 'stroke-miterlimit' :'10', fill: '#662D91', 'z-index': 10};
         $this.options.arrowNoseAttr = {stroke: '#662D91', fill: '#662D91', 'z-index': 10};
         $this.options.textBtnStyle = {'font-size': 17, fill: '#292424', stroke: 'none', 'font-family': 'Arial, Helvetica, sans-serif', 'font-weight': 500, 'cursor': 'pointer','text-anchor': 'start'};
         $this.options.textInfoStyle = {'font-size': 15, fill: '#292424', stroke: 'none', 'font-family': 'Arial, Helvetica, sans-serif', 'font-weight': 500,'text-anchor': 'start', 'opacity': 0};
         $this.options.textTitle = {'font-size': 16, fill: '#292424', stroke: 'none', 'font-family': 'Arial, Helvetica, sans-serif', 'font-weight': 500};
         $this.options.btnStyle1 = {stroke: '#d0d8fb', fill: '#d0d8fb', 'cursor': 'pointer'};
         $this.options.btnStyle2 = {stroke: '#f6d6a2', fill: '#f6d6a2', 'cursor': 'pointer'};
         $this.options.btnStyle3 = {stroke: '#faeed9', fill: '#faeed9', 'cursor': 'pointer'};
         $this.defaults = defaults;
         $this.name = pluginName;
         $this.init();
    };

    Plugin.prototype.init = function(){
        var mapBox = $($this.element);
        var titleTxt = mapBox.children('[data-role="mapTitle"]').html();
            mapBox.children('[data-role="mapTitle"]').remove();
        var mapList = mapBox.children('[data-role="mapList"]');
            mapBox.children('[data-role="mapList"]').remove();
        var mapImg1 = mapBox.children('[data-role="mapImg1"]');
            mapBox.children('[data-role="mapImg1"]').remove();
        var mapImg2 = mapBox.children('[data-role="mapImg2"]');
        mapBox.children('[data-role="mapImg2"]').remove();
        var mapCoordinates = mapBox.data('mapcoordinates');
        var btnOpen = $('.'+mapBox.data('btn')+'');
        var grayFon = createGreyFon();
        var closeBtn = createCloseBtn();
        var closeBtnH = getCloseBtnH(closeBtn);
        var box = document.getElementById('mapScene');
        var paper = Raphael(box);

        var mapImgSrc1 = mapImg1.attr('src');
        var mapImgSrc2 = mapImg2.attr('src');
        var newMapImg = new Image;
        var newMapImgHeight, newMapImgWidth;
            newMapImg.src = mapImgSrc1;
        newMapImg.onload = function(){
            newMapImgHeight = newMapImg.height;
            newMapImgWidth = newMapImg.width;
            var fons = {
                fon2: paper.image(mapImgSrc2, 0, 0, newMapImgWidth, newMapImgHeight),
                fon1: paper.image(mapImgSrc1, 0, 0, newMapImgWidth, newMapImgHeight)
            };
            fons.fon2.hide();
            fons.fon1.hide();
            var conturs = {
                places: paper.set(),
                body: paper.set(),
                nose: paper.set(),
                buttons: paper.set(),
                infotext: paper.set()
            };

            $.getJSON(mapCoordinates, function(mapJson){
                var arrowsObj = mapJson.arrows;
                var placesObj = mapJson.places;
                drawTitle(paper, titleTxt);
                drawButtons(conturs, paper, arrowsObj, mapList, placesObj, fons);
                drawPlaces(placesObj, conturs.places, paper);
                drawArrows(arrowsObj, conturs.body, conturs.nose, conturs.places, paper);
                closeBtn.on('click.closemap', null, {mapBox: mapBox, grayFon: grayFon, conturs: conturs, arrowsObj: arrowsObj, allGroups: allGroups, placesObj: placesObj, fons: fons}, closeMap);
            });
            var allGroups = getAllGroups(mapList);
            drawInfoText(conturs.infotext, mapList, paper);
            mapBox.addClass('pos').hide();
            btnOpen.on('click.openmap', null, {mapBox: mapBox, grayFon: grayFon, closeBtn: closeBtn, closeBtnH: closeBtnH, paper: paper, fons: fons}, openMap);
            $(window).resize(function(){
                if(mapBox.is(':visible')){
                    resizeScene(mapBox, paper, closeBtnH);
                }
            });
        }
    };

    $.fn[pluginName] = function(options){
        return this.each(function(){
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    };
})(jQuery, window, document);



















